function toggleBurgerMenu(e) {
    const navigationPanel = document.querySelector('nav');
    const iconBurger = btnBurger[0].firstChild;
    if (navigationPanel.classList.contains('burger')) {
        iconBurger.src = './images/icon_burger.png';
        iconBurger.removeAttribute('width');
        navigationPanel.classList.add('x1920');
        navigationPanel.classList.remove('burger');
    }
    else {
        iconBurger.src = './images/icon_cross.png';
        iconBurger.style.width = '18px';
        navigationPanel.classList.remove('x1920');
        navigationPanel.classList.add('burger');
    }
}