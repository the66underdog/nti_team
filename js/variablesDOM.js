const iconProfile = document.getElementsByClassName('li-icon-profile');
const btnBurger = document.getElementsByClassName('btn-burger');
const inputName = document.getElementById('name');
const inputNumber = document.getElementById('number');
const form = document.getElementsByTagName('form');

window.onload = () => {
    iconProfile[0].addEventListener('mouseenter', showProfileMenu);
    btnBurger[0].addEventListener('click', toggleBurgerMenu);
    form[0].addEventListener('submit', checkInputs);
}