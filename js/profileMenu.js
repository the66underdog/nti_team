function showProfileMenu(e) {
    const ulParent = e.currentTarget.parentElement;
    const profileMenu = ulParent.lastElementChild;
    profileMenu.style.cssText = `
        transform: translateX(calc(-50% + ${e.currentTarget.offsetWidth}px/2));
    `;
    profileMenu.classList.remove('hidden');
    iconProfile[0].addEventListener('mouseleave', hideProfileMenu);
    profileMenu.addEventListener('mouseleave', hideProfileMenu);
}

function hideProfileMenu(e) {
    const ulParent = e.currentTarget.parentElement;
    const profileMenu = ulParent.lastElementChild;
    if (!e.relatedTarget.classList.contains('profile-menu')) {
        profileMenu.classList.add('hidden');
        iconProfile[0].removeEventListener('mouseout', hideProfileMenu);
        profileMenu.removeEventListener('mouseleave', hideProfileMenu);
    }
}