const styles = document.styleSheets[0];

function checkInputs(e) {
    e.preventDefault();
    let msg = '';
    const [input1, msg1] = [...checkInputName()];
    if (input1) {
        var [input2, msg2] = [...checkInputNumber()];
        if (input2) {
            styles.insertRule(`#btn-order+label:after {
                content: 'Форма отправлена +';
                display: block;
                font-size: 20px;
                color: lightgreen;
                margin-top: 16px;
            }`, 1);
        }
        else {
            msg = msg2;
        }
    }
    else {
        msg = msg1;
    }
    if (!(input1 && input2)) {
        styles.insertRule(`#btn-order+label:after {
            content: '${msg}';
            display: block;
            font-size: 20px;
            color: red;
            margin-top: 16px;
        }`, 1);
    }
    setTimeout(() => {
        styles.deleteRule(1);
    }, 2000);
}

function checkInputName() {
    if (/\d/.test(inputName.value)) { // исключаем цифры
        return [false, 'Имя: В имени не может быть цифр -'];
    }
    if (/[^\w\-а-я ]/i.test(inputName.value)) { //исключаем знаки препинания (кроме - и пробела), добавляем кириллицу
        return [false, 'Имя: В имени не может быть знаков препинания -'];
    }
    return [true, ''];
}

function checkInputNumber() {
    if (/^[^\D78]/.test(inputNumber.value)) { // номер начинается только на 7 и 8
        return [false, 'Телефон: 7/8ХХХХХХХХХХ формата -'];
    }
    if (inputNumber.value.length !== 11) // проверка длины номера
        return [false, 'Телефон: номер состоит из 11 цифр -']
    if (/[^\d]/.test(inputNumber.value)) { // исключаем всё, кроме цифр
        return [false, 'Телефон: только цифры -'];
    }
    return [true, ''];
}